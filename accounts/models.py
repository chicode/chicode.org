
from django.contrib.auth.models import AbstractUser
from django.db import models

class CustomUser(AbstractUser):
	#username = models.CharField(unique=True)
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	email = models.EmailField(max_length=50, unique=True)
	race = models.CharField(max_length=50)
	gender = models.CharField(max_length=50)