from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser

class CustomUserCreationForm(UserCreationForm):
	#username = forms.CharField(unique=True)
	email=forms.EmailField(max_length=50)
	class Meta(UserCreationForm.Meta):
		model = CustomUser
		fields = ('first_name', 'last_name', 'email', 'race', 'gender', 'username',)

class CustomUserChangeForm(UserChangeForm):
	#username = forms.CharField(unique=True)
	email=forms.EmailField(max_length=50)
	class Meta:
		model = CustomUser
		fields = ('first_name', 'last_name', 'email', 'race', 'gender', 'username',)