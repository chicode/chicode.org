from django.shortcuts import render

# Create your views here.
def home(request):
	context = {}
	template = 'info/home.html'
	return render(request, template, context)
def about(request):
	context = {}
	template = 'info/about.html'
	return render(request, template, context)
def faq(request):
	context = {}
	template = 'info/faq.html'
	return render(request, template, context)